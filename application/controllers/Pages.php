<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pages extends CI_Controller {

    /**
     * Displaying pages on root of the website
     * @param string $page  May be (index / about-us / contact ) etc
     * @param content $content
     */
    public function view($page = 'index', $content = null) {
        if (in_array($page, array('home', 'website-design', 'our-services'))) {
            redirectThis($page);
        }
        if (!file_exists(APPPATH . '/views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $data['site_title'] = 'My Web Site Name';

        $data['page'] = $page;
        if ($page == 'index') {
            $data['title'] = "My Web Site Name";
        } else if ($page == "not_found") {
            $data['title'] = "404 - Page not found &#8226; My Web Site Name";
        } else {
            $data['title'] = str_replace('-', ' ', ucwords($page)) . " . My Web Site Name"; // Capitalize the first letter
        }

        //Meta tag details
        $data['description'] = "";
        $data['keywords'] = "";

        if ($page == 'index') {
            $data['title'] = "My Web Site Name";
            $data['description'] = "My Web Site Name is the best web development company in US & UK";
            $data['keywords'] = "";
        }
        
        if ($page == 'thank-you') {
            $data['title'] = 'Thank you';
            $data['description'] = "";
            $data['keywords'] = "";
        }

        $detect = new Mobile_Detect();
        $data['isMobile'] = $detect->isMobile();
        $data['isTab'] = $detect->isTablet();
        $data['isDesktop'] = (!$data['isMobile'] && !$data['isTab']);

        $data['header'] = $this->load->view('templates/header', $data, true);
        $data['menu'] = $this->load->view('templates/menu', $data, true);
        $data['footer'] = $this->load->view('templates/footer', $data, true);
        $this->load->view('pages/' . $page, $data);
    }
}

?>