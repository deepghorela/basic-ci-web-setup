<?php

/**
 * Description of Form
 *
 * @author TheBlackPearl
 */
class Form extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->input->is_ajax_request()) {
            echo json_encode(array('errors' => array('Unauthorised request found')));
            exit;
        }
    }

    public function enquiry() {
        $data = $this->security->xss_clean($_POST);
        if (!empty($data)) {
            $name = addslashes($_POST['name']);
            $service = addslashes($_POST['service']);
            $email = addslashes($_POST['email']);
            $phone = addslashes($_POST['phone']);
            $msg = addslashes($_POST['message']);
            $page = addslashes($_POST['ref']);
            $website = isset($_POST['website']) ? $_POST['website'] : 'NA';

            $array = array(
                'name' => $name,
                'services' => $service,
                'email' => $email,
                'tel' => $phone,
                'message' => $msg,
                'page' => $page,
                'website' => $website,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'recordedOn' => date('Y-m-d H:i:s')
            );
            $error = 0;
            foreach ($array as $k => $field) {
                //If any field is empty
                if (in_array($k, array('name', 'email', 'message', 'tel')) && empty($field)) {
                    $error = 1;
                }
            }
            if ($error) {
                echo json_encode(array('status' => false, 'errors' => 'Please fill all fields.'));
            } else {
                $page = $page == 'index' ? "Home Page" : $page;
                $subject = !empty($page) ? 'Enquiry received from ' . ucwords(str_replace('-', ' ', $page))." page" : 'Enquiry received';
                //Record to DB
                $this->load->model('enquiry');
                $this->enquiry->recordEnquiry($array); //Record saved


                require_once APPPATH . 'libraries/swiftmailer/swift_required.php';
                // Create the mail transport configuration
                $transport = Swift_MailTransport::newInstance();

                // Create the message
                $message = Swift_Message::newInstance();
                $message->setTo(array(
                    'info@letsrundigital.com' => 'Web Admin',
                    'deepakg@letsrundigital.com' => 'Deep Ghorela'
                ));
                //'info@letsrundigital.com' => 'Web Admin',
                //'deepakg@letsrundigital.com' => 'Deep Ghorela'
                $message->setContentType("text/html");
                $message->setSubject($subject);

                //Fetch template and replace content
                $content = file_get_contents(FCPATH . 'application/views/templates/email/enquiry.html');
                $content = str_replace('_NAME_', $name ? $name : 'Not available', $content);
                $content = str_replace('_COMPANY_', $service ? $service : 'Not available', $content);
                $content = str_replace('_EMAIL_', $email ? $email : 'Not available', $content);
                $content = str_replace('_TELEPHONE_', $phone ? $phone : 'Not available', $content);
                $content = str_replace('_MESSAGE_', $msg ? $msg : 'Not available', $content);
                //Template ready

                $message->setBody($content);
                $message->setFrom("no-reply@" . getHostAddress(), "Let's Run Digital");

                // Send the email
                $mailer = Swift_Mailer::newInstance($transport);
                if ($mailer->send($message)) {
                    echo json_encode(array('status' => true, 'message' => 'Enquiry received successfully'));
                } else {
                    echo json_encode(['status'=>false, 'message' => 'Unable to record your message. Please try again later']);
                }
            }
        } else {
            echo json_encode(['status'=>false, 'message' => 'Please fill all fields.']);
        }
    }

}
