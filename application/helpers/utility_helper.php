<?php

/**
 * These helpers will provide you some basic verify / creation key features or functionalities.
 * Created On 04-Dec-2016
 * @author Deep Ghorela <ghoreladeep@gmail.com>
 */

/**
 * Generate Random string of defined length
 * @param int $length   Default length is 16
 * @return string       Random Sting
 */
function genPermToken($length = 16) {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $str = array(); //remember to declare $str as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < $length; $i++) {
        $n = rand(0, $alphaLength);
        $str[] = $alphabet[$n];
    }
    $pureString = implode($str); //turn the array into a string
    return $pureString;
}

/**
 * Check if email address is valid
 * @param string $email
 * @return boolean
 */
function isValidEmail($email) {
    return !filter_var($email, FILTER_VALIDATE_EMAIL) === false ? true : false;
}

/**
 * Check if phone number is valid as per indian standards
 * @param string $email
 * @return boolean
 */
function isValidMobile($phone) {
    return is_numeric($phone) && strlen($phone) == 10 ? true : false;
}

/**
 * Get user IP
 * @return string
 */
function userIp() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if (isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if (isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if (isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

/**
 * Get Small String from text
 * @param type $large_string
 * @param int $length
 * @return string
 */
function getSmallSting($large_string, $length) {
    if (empty($length) || $length == "") {
        $length = 50;
    }
    $string = ucfirst(strtolower($large_string));
    // strip tags to avoid breaking any html
    $string = strip_tags($string);
    if (strlen($string) > $length) {
        // truncate string
        $stringCut = substr($string, 0, $length);
        // make sure it ends in a word so assassinate doesn't become ass...
        $string = substr($stringCut, 0, strrpos($stringCut, ' ')) . "...";
    }
    return $string;
}

/**
 * Money Format of India
 * @param string $num
 * @return string
 */
function moneyFormatIndia($num) {
    $explrestunits = "";
    if (strlen($num) > 3) {
        $lastthree = substr($num, strlen($num) - 3, strlen($num));
        $restunits = substr($num, 0, strlen($num) - 3); // extracts the last three digits
        $restunits = (strlen($restunits) % 2 == 1) ? "0" . $restunits : $restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
        $expunit = str_split($restunits, 2);
        for ($i = 0; $i < sizeof($expunit); $i++) {
            // creates each of the 2's group and adds a comma to the end
            if ($i == 0) {
                $explrestunits .= (int) $expunit[$i] . ","; // if is first value , convert into integer
            } else {
                $explrestunits .= $expunit[$i] . ",";
            }
        }
        $thecash = $explrestunits . $lastthree;
    } else {
        $thecash = $num;
    }
    return $thecash; // writes the final format where $currency is the currency symbol.
}

/**
 * Send email
 * @param type $data array('to'=>target@gmail.com, 'toName'=>target name, 'from'=>from@gmail.com, 'fromName'=>fromName, 'subject'=>subject name, 'body'=>HTML Body of email)
 * @return boolean
 */
function sendEmail($data) {
    require_once APPPATH . 'libraries/swiftmailer/swift_required.php';
    // Create the mail transport configuration
    $transport = Swift_MailTransport::newInstance();

    // Create the message
    $message = Swift_Message::newInstance();
    $message->setTo(array(
        $data['to'] => $data['toName']
    ));
    $message->setContentType("text/html");
    $message->setSubject($data['subject']);
    $message->setBody($data['body']);
    $message->setFrom($data['from'], $data['fromName']);

    // Send the email
    $mailer = Swift_Mailer::newInstance($transport);
    return $mailer->send($message);
}

/**
 * Get host address from URL
 * @return string
 */
function getHostAddress() {
    $pieces = parse_url("http://" . $_SERVER['HTTP_HOST']);
    $domain = isset($pieces['host']) ? $pieces['host'] : '';
    if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
        return $regs['domain'];
    }
    return false;
}

/**
 * Generate SEO Friendly titles
 * @param type $title
 * @return type
 */
function genSeoFriendlyTitles($title) {
    $title = trim($title);
    $replace_what = array('"', '.', '&', '%', '(', ')', '/', '  ', ' - ', ', ', ',', ' ');
    $replace_with = array('', '', '', '', '', '', '', ' ', '-', ',', '-', '-');
    $title = strtolower($title);
    $title = str_replace($replace_what, $replace_with, $title);
    return $title;
}

/**
 * Get current Page URL
 * @param boolean $encode  True if you need output encoded
 * @return string
 */
function getPageUrl($encode = FALSE) {
    $pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    if ($encode === TRUE) {
        $pageURL = urlencode($pageURL);
    }

    return $pageURL;
}

/**
 * Geneate captcha number
 * @return type
 */
function gen_captcha() {
    $symbols = '123456789';
    $string = '';
    for ($i = 0; $i < 6; $i++) {
        $string .= substr($symbols, mt_rand(0, strlen($symbols)), 1);
    }
    return $string;
}

/**
 * Draw Captcha 
 * @param string $security_code Length 6 digits
 */
function draw_captcha($security_code) {

    //Set the image width and height
    $width = 120;
    $height = 40;

    //Create the image resource
    $image = ImageCreate($width, $height);
    if (function_exists('imageantialias')) {
        imageantialias($image, true);
    }

    //We are making three colors, white, black and gray
    $white = ImageColorAllocate($image, 255, 255, 255);
    $black = ImageColorAllocate($image, 247, 114, 25);
    $grey = ImageColorAllocate($image, 204, 204, 204);
    $ellipsec = ImageColorAllocate($image, 247, 114, 25);

    //Make the background black
    ImageFill($image, 0, 0, $black);
    imagefilledellipse($image, 56, 15, 30, 17, $ellipsec);

    //Add randomly generated string in white to the image
    ImageString($image, 5, 35, 10, $security_code, $white);


    //Tell the browser what kind of file is come in
    header("Content-Type: image/jpeg");

    //Output the newly created image in jpeg format
    ImageJpeg($image);

    //Free up resources
    ImageDestroy($image);
}

/**
 * Get a valid string after removing special characters
 * @param string $string
 * @return string
 */
function validString($string) {
    // remove html tags
    $string = strip_tags($string);
    // remove url from string
    return preg_replace("/[a-zA-Z0-9\_\-]*[\.][a-zA-Z0-9\?\=\&\-\_\/\.\-]+/", "", $string);
}

/**
 * Check if string is json or not
 * @param string $string
 * @return boolean
 */
function isJson($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}

/**
 * 
 * @param string $string
 * @return array
 */
function urlFromString($string) {
    preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $string, $match);
    return $match[0];
}

/**
 * 
 * @param string $datetime
 * @param type $full
 */
function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );

    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full)
        $string = array_slice($string, 0, 1);
    //return $string ? implode(',',$string) . ' ago' : 'just now';
    return $string ? current($string) . ' ago' : 'just now';
}

function getTweets() {
    require_once(APPPATH . "libraries/TwitterProxy.php");
    // Create a Twitter Proxy object from our twitter_proxy.php class
    // Twitter OAuth Config options
    $oauth_access_token = '';
    $oauth_access_token_secret = '';
    $consumer_key = '';
    $consumer_secret = '';
    $user_id = '';
    $screen_name = '';
    $count = 3;

    $twitter_url = 'statuses/user_timeline.json';
    $twitter_url .= '?user_id=' . $user_id;
    $twitter_url .= '&screen_name=' . $screen_name;
    $twitter_url .= '&count=' . $count;

    $twitter_proxy = new TwitterProxy(
            $oauth_access_token, // 'Access token' on https://apps.twitter.com
            $oauth_access_token_secret, // 'Access token secret' on https://apps.twitter.com
            $consumer_key, // 'API key' on https://apps.twitter.com
            $consumer_secret, // 'API secret' on https://apps.twitter.com
            $user_id, // User id (http://gettwitterid.com/)
            $screen_name, // Twitter handle
            $count       // The number of tweets to pull out
    );
    $tweets = $twitter_proxy->get($twitter_url);
    $arr = json_decode($tweets);
    foreach ($arr as &$tweet) {
        $content = $tweet->text;
        $urls = urlFromString($content);
        $t[] = array(
            'url' => $tweetUrl = end($urls),
            'timeAgo' => time_elapsed_string($tweet->created_at),
            'content' => addslashes(rtrim($content, $tweetUrl))
        );
    }
    return $t;
}

/**
 * Remove unwanted space from 
 * @param type $content
 * @return type
 */
function stripSpaces($content) {
    return preg_replace('/\s+/', ' ', $content);
}

/**
 * Encode URL from encryption in  codeigninter, because CI doesnot allow + / = characters in URL
 * @param type $str
 * @return type
 */
function encodeMyWay($str) {
    return str_replace(array('+', '/', '='), array('-', '_', '~'), $str);
}

/**
 * Decode URL from encryption in  codeigninter, because CI doesnot allow + / = characters in URL
 * @param type $str
 * @return type
 */
function decodeMyWay($str) {
    return str_replace(array('-', '_', '~'), array('+', '/', '='), $str);
}

/**
 * moveElement of array
  | How does it work:
  | First: remove/splice the element from the array
  | Second: splice the array into two parts at the position you want to insert the element
  | Merge the three parts together
  | @param type $array
  | @param int $a    From position
  | @param int $b    To position
 */
function moveElement(&$array, $a, $b) {
    $p1 = array_splice($array, $a, 1);
    $p2 = array_splice($array, 0, $b);
    $array = array_merge($p2, $p1, $array);
}

/**
 * Get a clean alphanumeric string
 * @param string $string
 * @return string
 */
function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

/**
 * Encode some content that is also acceptable for URLs means we can pass this encrypted code in URL
 * @param string $string
 * @return string
 */
function enc($string) {
    $CI = & get_instance();
    return encodeMyWay($CI->encrypt->encode($string, VG_SALT));
}

/**
 * Decode the encrypted URL
 * @param type $string
 * @return type
 */
function dec($string) {
    $CI = & get_instance();
    return $CI->encrypt->decode(decodeMyWay($string), VG_SALT);
}

/**
 * Converts bytes into human readable file size. 
 * 
 * @param string $bytes 
 * @return string human readable file size (2,87 Мб)
 */
function fileSizeConvert($bytes) {
    $bytes = floatval($bytes);
    $arBytes = array(
        0 => array(
            "UNIT" => "TB",
            "VALUE" => pow(1024, 4)
        ),
        1 => array(
            "UNIT" => "GB",
            "VALUE" => pow(1024, 3)
        ),
        2 => array(
            "UNIT" => "MB",
            "VALUE" => pow(1024, 2)
        ),
        3 => array(
            "UNIT" => "KB",
            "VALUE" => 1024
        ),
        4 => array(
            "UNIT" => "B",
            "VALUE" => 1
        ),
    );
    foreach ($arBytes as $arItem) {
        if ($bytes >= $arItem["VALUE"]) {
            $result = $bytes / $arItem["VALUE"];
            $result = str_replace(".", ",", strval(round($result, 2))) . " " . $arItem["UNIT"];
            break;
        }
    }
    return $result;
}

function find_filesize($file) {
    if (substr(PHP_OS, 0, 3) == "WIN") {
        exec('for %I in ("' . $file . '") do @echo %~zI', $output);
        $return = $output[0];
    } else {
        $return = filesize($file);
    }
    return fileSizeConvert($return);
}
