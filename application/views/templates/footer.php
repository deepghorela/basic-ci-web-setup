<!--footer start-->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center copyright">
                <p><?php echo $site_title;?> Address, Street, City, District, State. Contact: +XX-XXXXXXXXX</p>
                <p> Copyright &copy; <?php echo date('Y'); ?> <a href="<?php echo base_url();?>"><?php echo $site_title;?></a>. All rights reserved!.</p>
            </div>
        </div>
    </div>
</footer>
<!--footer end-->
<script type="text/javascript">
    function addScript(src) {
        var script = document.createElement('script');
        script.setAttribute('type', 'text/javascript');
        script.setAttribute('src', src);
        $('head').append(script);
    }
    function addCss(href) {
        var link = document.createElement('link');
        link.setAttribute('type', 'text/css');
        link.setAttribute('rel', 'stylesheet');
        link.setAttribute('href', href);
        $('title').before(link);
    }
    $(window).bind('load', function () {
        setTimeout(function () {
            $(".web-preloader").fadeOut('1000');
        }, 1000);
    });
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type='text/javascript'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js" type='text/javascript'></script>
<?php if (base_url() != 'http://localhost/lets/') { ?>
    <script type="text/javascript">!function (a, b, c, d, e, f, g) {
        a.GoogleAnalyticsObject = e, a[e] = a[e] || function () {
            (a[e].q = a[e].q || []).push(arguments)
        }, a[e].l = 1 * new Date, f = b.createElement(c), g = b.getElementsByTagName(c)[0], f.async = 1, f.src = d, g.parentNode.insertBefore(f, g)
    }(window, document, "script", "https://www.google-analytics.com/analytics.js", "ga"), ga("create", "UA-47859197-10", "auto"), ga("send", "pageview");</script>
<?php } ?>