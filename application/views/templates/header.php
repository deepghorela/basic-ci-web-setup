<title><?php echo $title; ?></title>
<meta charset=utf-8 />
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="description" content="<?php echo $description; ?>">
<meta name="keywords" content="<?php echo $keywords; ?>">
<meta name="robots" content="index, follow">
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="<?php echo $title; ?>" />
<meta name="twitter:image" content="<?php echo base_url(); ?>assets/images/logo.png" />
<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:type" content="business.business" />
<meta property="og:url" content="<?php echo base_url(); ?>" />
<meta property="og:image" content="<?php echo base_url(); ?>assets/images/logo.png" />
<meta property="og:author" content="<?php echo $site_title; ?> <?php echo base_url(); ?>" />
<meta property="og:site_name" content="<?php echo $title; ?>" />
<link rel="dns-prefetch" href="<?php echo base_url(); ?>">
<meta property="og:description" content="<?php echo $title; ?> " />
<meta name="author" content="<?php echo $site_title; ?>">
<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/images/favicon.png" />
<meta itemprop="image" content="<?php echo base_url(); ?>assets/img/logo.png" />
<link rel="alternate" hreflang="en-US" href="<?php echo base_url(); ?>" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />
<link rel="stylesheet" type="text/css" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700" >
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/custom.css">
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.min.js" ></script>



