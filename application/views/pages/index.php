<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo isset($header) ? $header : ''; ?>
    </head>
    <body class="home">
        <div class="layout">
            <?php echo isset($menu) ? $menu : ''; ?>
            <section class="text-center first-section">
                <h1><?php echo $site_title;?></h1>
            </section>
            <section class="text-center">
                <h2><?php echo $site_title;?></h2>
            </section>
            <?php echo isset($footer) ? $footer : ''; ?>
        </div>
    </body>
</html>
