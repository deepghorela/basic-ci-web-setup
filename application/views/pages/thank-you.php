<!DOCTYPE html>
<html lang="en-US">
    <head>
        <?php if(isset($header)){
            echo $header;
        }
        ?>
        <style>.specialBlock{height:20vh;padding-top:0px;background: #fff}
            .thank-you{width:50%;margin-left: 25%;padding:20px;min-height:10vh;}
            .thank-you h2{margin-bottom: 5px;}.thank-you p{font-size: 1.3em;}
            .thumbLarge{color:#fff;font-size: 35px;}.startscroll{min-height: 250px;}</style>
    </head>
    <body class="basic">
        <?php
        if (isset($menu)) {
            echo $menu;
        }
        ?>
        <section class="startscroll">
            <div class="inner-banner specialBlock">
                <div class="thank-you text-center">
                    <h1 class='fa-3x'><i class="fa fa-thumbs-o-up text-success"></i> <br> Thank You</h1>
                    <p>We have received your query and soon will get in touch with you.<br>Stay connected with us.</p>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <?php echo isset($footer) ?  $footer : '';?>

        <?php if (base_url() != 'http://letsrundigital.dev/') { ?>
        <script type="text/javascript">!function(a,b,c,d,e,f,g){a.GoogleAnalyticsObject=e,a[e]=a[e]||function(){(a[e].q=a[e].q||[]).push(arguments)},a[e].l=1*new Date,f=b.createElement(c),g=b.getElementsByTagName(c)[0],f.async=1,f.src=d,g.parentNode.insertBefore(f,g)}(window,document,"script","https://www.google-analytics.com/analytics.js","ga"),ga("create","UA-47859197-10","auto"),ga("send","pageview");</script>
        <?php } ?>
    </body>
</html>
